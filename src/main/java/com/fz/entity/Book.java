package com.fz.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by webrx on 2017-09-05.
 */
@Data
public class Book implements Serializable{
    private int id;
    private String name;
    private double price;
    private Date bdate;
}
