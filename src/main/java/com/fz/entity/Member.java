package com.fz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by webrx on 2017-09-01.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member {
    private int id;
    private String account;
    private String pass;
    private String name;
    private List<Order> order;
}
