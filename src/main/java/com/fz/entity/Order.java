package com.fz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by webrx on 2017-09-01.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private int oid;
    private String ono;
    private double omoney;
    private int omid;
}
