package com.fz.mapper;

import com.fz.entity.Book;
import com.fz.entity.Member;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by webrx on 2017-08-31.
 */
@CacheNamespace
public interface BookMapper {
    @Select("select * from db_book where id=#{id}")
    public Book queryById(int id);

    //@Select("select * from db_book") @Options(useCache = true)
    @Select("select * from db_book")
    public List<Book> queryAll();
}
