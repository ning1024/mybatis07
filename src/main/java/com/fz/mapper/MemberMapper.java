package com.fz.mapper;

import com.fz.entity.Member;

import java.util.List;

/**
 * Created by webrx on 2017-08-31.
 */
public interface MemberMapper {
    public Member queryById(int id);
    public Member queryByInt(int id);
    public List<Member> queryAll();

    public Member queryByIdLazy(int id);
}
