import com.fz.entity.Book;
import com.fz.entity.Member;
import com.fz.entity.Order;
import com.fz.mapper.BookMapper;
import com.fz.mapper.MemberMapper;
import mybatis.MyHelper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import java.util.List;

/**
 * Created by webrx on 2017-09-05.
 */
public class Demo {

    @Test
    public void book(){
        MyHelper mh = new MyHelper();

//        SqlSession s = mh.getSession();
//        BookMapper b = s.getMapper(BookMapper.class);
        //List<Book> books = b.queryAll();
//        Book b1 = b.queryById(1);
//        s.clearCache();
//        Book b2 = b.queryById(1);
//        System.out.println(b1==b2);
        //测试二级缓存
        SqlSessionFactory sf = mh.getSqlSessionFactory();
        SqlSession s1 = sf.openSession();
        List<Book> books = s1.getMapper(BookMapper.class).queryAll();
        s1.close();
        SqlSession s2 = sf.openSession();
        List<Book> booksa = s2.getMapper(BookMapper.class).queryAll();

    }
    @Test
    public void aa(){
        MyHelper mh = new MyHelper();
        SqlSession s = mh.getSession();
        MemberMapper m = s.getMapper(MemberMapper.class);
        Member mem = m.queryByIdLazy(1);
        System.out.println(mem.getName());
        System.out.println(mem.getAccount());
        // System.out.println(mem.getPass());
        // System.out.println(mem.getOrder());

        System.out.println(".......订单信息.......");
        List<Order> os = mem.getOrder();
        System.out.println(os);
      /*    List<Member> list = m.queryAll();
        System.out.println("............");
       for(Member mm : list){
           System.out.println(mm.getName());
           System.out.println(mm.getOrder());
                   }*/
        s.commit();
    }
}
